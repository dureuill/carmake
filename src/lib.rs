use cmake::Config;
use semver::Version;
use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use std::env;
use std::path::{Path, PathBuf};

#[derive(Deserialize, Serialize, Clone)]
struct Dep {
    name: String,
    version: Version,
    cmake_config_path: PathBuf,
    deps: Vec<PathBuf>,
}

impl Dep {
    fn from_env(cmake_config_path: PathBuf, deps: Vec<PathBuf>) -> Self {
        Dep {
            name: env::var("CARGO_PKG_NAME").expect("Package name not set"),
            version: Version::parse(
                &env::var("CARGO_PKG_VERSION").expect("Could not retrieve package version"),
            )
            .expect("CARGO_PKG_VERSION is not a valid package version"),
            cmake_config_path,
            deps,
        }
    }

    fn from_path(dep: &Path) -> Self {
        let dep = std::fs::read(dep).expect("Could not read file");
        toml::from_slice(&dep).expect("Could not deserialize dependency")
    }
}

fn direct_deps_from_env() -> Vec<PathBuf> {
    const PREFIX: &'static str = "DEP_";
    const SUFFIX: &'static str = "_CARMAKE";

    env::vars()
        .filter(|(key, _)| key.starts_with(PREFIX) && key.ends_with(SUFFIX))
        .map(|(_, value)| PathBuf::from(&value))
        .collect()
}

fn populate_all_deps(direct_deps: &[PathBuf]) -> HashMap<String, Dep> {
    let mut deps_to_visit: Vec<_> = direct_deps
        .into_iter()
        .map(|path| Dep::from_path(path))
        .collect();
    let mut deps = HashMap::new();
    while let Some(dep) = deps_to_visit.pop() {
        deps_to_visit.extend(
            dep.deps
                .iter()
                .map(|indirect_dep| Dep::from_path(indirect_dep)),
        );
        deps.entry(dep.name.clone())
            .and_modify(|existing_dep: &mut Dep| assert_eq!(existing_dep.version, dep.version))
            .or_insert(dep);
    }
    deps
}

fn serialize_package_dep(path: impl AsRef<std::path::Path>, dep: &Dep) {
    std::fs::write(path, toml::to_string(dep).expect("Could not serialize dep"))
        .expect("Could not write output file");
}

pub fn do_all(path: impl AsRef<Path>) {
    let deps = direct_deps_from_env();
    let all_deps = populate_all_deps(&deps);
    let mut config = Config::new(path).find_carmake(all_deps).build();
    config.push("output");
    let dep = Dep::from_env(config, deps);
    let out_path = std::env::var("OUT_DIR").expect("OUT_DIR not set.") + "/carmake.toml";
    serialize_package_dep(&out_path, &dep);
    println!("cargo:carmake={}", &out_path);
}

trait Carmake {
    fn find_carmake(&mut self, all_deps: HashMap<String, Dep>) -> &mut Self;
}

impl Carmake for Config {
    fn find_carmake(&mut self, all_deps: HashMap<String, Dep>) -> &mut Self {
        for (key, dep) in all_deps {
            let dep_name = key + "_DIR";
            self.define(dep_name, dep.cmake_config_path);
        }
        self
    }
}
