include(CMakeFindDependencyMacro)

# add you `find_dependency(foo REQUIRED)` here
# one line for each `find_package` that should be
# found by carmake

find_dependency(hallo REQUIRED)
find_dependency(hello REQUIRED)

#

include("${CMAKE_CURRENT_LIST_DIR}/dep.cmake")

