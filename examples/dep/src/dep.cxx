#include "../include/dep.h"
#include <hallo.h>
#include <hello.h>

std::string foo() {
	return say_hallo("from cmake and ") + say_hello("from cargo");
}
