#include "../include/hallo.h"

std::string say_hallo(std::string_view name) {
	return std::string("Hallo, ").append(name);
}
