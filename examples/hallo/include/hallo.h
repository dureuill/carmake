#pragma once

#include <string>
#include <string_view>

std::string say_hallo(std::string_view name);
