#pragma once

#include <string>
#include <string_view>

std::string say_hello(std::string_view name);
