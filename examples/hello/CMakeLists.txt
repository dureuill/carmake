cmake_minimum_required(VERSION 3.1)
project("Hello" CXX)

add_library(hello "src/hello.cxx")

target_include_directories(hello
	PUBLIC $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
	 $<INSTALL_INTERFACE:output/include>
	)

target_compile_features(hello PRIVATE cxx_std_17)

install(TARGETS hello EXPORT hello DESTINATION "output/lib")
install(DIRECTORY include DESTINATION "output")

install(EXPORT hello FILE hello.cmake DESTINATION "output/")

install(FILES "${CMAKE_CURRENT_SOURCE_DIR}/cmake/hello-config.cmake" DESTINATION "output/")
