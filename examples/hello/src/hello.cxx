#include "../include/hello.h"

std::string say_hello(std::string_view name) {
	return std::string("Hello, ").append(name);
}
