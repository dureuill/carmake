#include <iostream>

#include <dep.h>
#include <hallo.h>
#include <hello.h>

int main() {
	std::cout << "foo: " << foo() << std::endl;
	std::cout << "hallo: " << say_hallo("Intel the Beagle") << std::endl;
	std::cout << "hello: " << say_hello("Heiko the cat") << std::endl;
	return 0;
}
