Examples
========

This directory contains four "carmake-ready" packages:

* `hallo`, a C++ lib
* `hello`, another C++ lib
* `dep`, a C++ lib that depends on `hello` and `hallo`
* `twodep`, a C++ binary that depends on `dep`, `hello` and `hallo`

You can try for instance to build `twodep` by going into `twodep`'s directory, and then:
```bash
$ cargo build
```

You can then run the produced binary:

```bash
target/debug/build/twodeps-52b041af1d3285cc/out/bin/greet
```
