cargo, "new" C++ package manager
================================

This repository contains `carmake`, a simple rust crate that allows to use rust's [`cargo`](https://doc.rust-lang.org/cargo/) as a C++ package manager.

To do so, `carmake` leverages the excellent [`cmake`](https://crates.io/crates/cmake) crate to build C++ project from their existing `CMakeLists.txt` and put the result in an `output` directory.
`carmake` provides the necessary glue so that a package using `carmake` can depend on other C++ libraries instrumented with `carmake`.

`carmake` has been designed for packages containing only C++ code.

See also the [examples](examples).

Constraints on the C++ package
==============================

1. Must use (modern) CMake
2. Must have only dependencies that are:
    1. directly vendored in the repository
    2. other `carmake` packages, *or*
    3. system dependencies.
3. Must target-install ONE target inside of an `output/` directory.
4. Must install-export a `foo.cmake` to the `output/` directory.
5. Must file-install a dep-config.cmake that performs `find_dependency` for each `find_package` that must be found by carmake to the `output/` directory.

How to use
==========

Starting from a C++ package with a `CMakeLists.txt` that installs a target `foo`, in a directory called `foo`.


1. Initialize a `cargo` project:
```bash
$ cargo init --lib .
```
This should create the basic structure of a `cargo` project, such as a `Cargo.toml` and a `src` directory (if your C++ library doesn't already has one).

2. Edit your `Cargo.toml` like the following:
```toml
[package]
name = "foo"
# [...]
links = "foo" # required to signal your dependencies that you use carmake

[build-dependencies]
carmake = "0.1"  # carmake dependency
```

3. Add a `build.rs` file at the root of the `foo` directory:
```rs
fn main() {
    carmake::do_all(".")
}
```

4. Run `cargo build`. The target should appear in a directory looking like `target/debug/build/foo-f00545143ececc5a/out/output/`

5. You can now do the same for the packages that might depend on `foo`, and add the dependency to foo in their `Cargo.toml`:

```toml
[package]
name = "bar"
# [...]
links = "bar"

[build-dependencies]
carmake = { path = "../../carmake" }

[dependencies]
foo = "0.1"
```

TODO
====

1. Proper error handling. Some errors are expected to keep simply using `expect` (what are we gonna do if `CARGO_PKG_NAME` is not set?), but other errors should definitely have a custom handling (IO errors around finding the TOML files, for instance...).
2. Documentation.
3. Naming. The `do_all` function has a less than stellar name
4. Give access to the various parts of carmake, so user can e.g. configure the CMake (only accessible endpoint is `do_all` fn at the moment)
5. cmake configuration support. Can be achieved with cargo features.
6. Test on real C++ packages!
7. Compiled targets appear in a sub-sub-sub-sub directory of `target/debug`. Reduce that depth!
8. We still need a `src/lib.rs` or `src/main.rs` (only possible for binaries), otherwise `cargo build` doesn't work.
